<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * CakePHP must die!!!!!
 * Fuck Structure!!
 * WTF!!! WTF!!!! WTF!!!!!!!!!!!!!!
 */

/*
 * CsvInport
 */
class Csvimport extends CI_Controller
{

    public function index($mes = 'aaaa')
    {
        echo $mes;
        $result = $this->db->get('admin_users');
        var_dump($result->result());
        // $this->load->view('welcome_message');
    }



    /*
     * 素材データ取り込み
     * php index.php csvimport order iwate_order.csv 1
     * place -> 1(岩手) 2(青森)
     */
    public function order( $CSV_PATH = '' , $PLACE_ID = 1)
    {
        // データベース初期化
        // $this->load->database();
        if ( $this->db->table_exists('suppliers') )
        {
            $this->db->query('delete from suppliers');
            $this->db->query('drop table suppliers');
        }
        if ( $this->db->table_exists('foodstuff_orders') )
        {
            $this->db->query('delete from foodstuff_orders');
            $this->db->query('drop table foodstuff_orders');
        }
        if ( $this->db->table_exists('units') )
        {
            $this->db->query('delete from units');
            $this->db->query('drop table units');
        }

$SQL1 = "
CREATE TABLE suppliers
(
id int unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
place_id int unsigned NOT NULL COMMENT '拠点ID',
supplier_name text NOT NULL COMMENT '発注先名称',
created datetime NOT NULL COMMENT '作成時刻',
updated datetime NOT NULL COMMENT '更新時刻',
deleted datetime COMMENT '削除時刻',
-- 削除フラグ(0=未削除,1=削除済み)
is_deleted tinyint unsigned DEFAULT 0 NOT NULL COMMENT '削除フラグ',
PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '仕入先マスタ' DEFAULT CHARACTER SET utf8;
";

$SQL2 = "
CREATE TABLE foodstuff_orders
(
id int unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
place_id int unsigned NOT NULL COMMENT '拠点ID',
supplier_id int unsigned NOT NULL COMMENT '仕入先ID',
order_name text COMMENT '発注用名称',
-- 『調理に必要な量÷発注時最小単位在庫量=発注数量』となる
order_minimum_amount float DEFAULT 1 NOT NULL COMMENT '発注時最小単位在庫量',
order_unit_id int unsigned NOT NULL COMMENT '単位ID(発注用)',
-- 0:無効,0<:アラートを表示する期間(発注日からメニュー日まで)
notify_ago int DEFAULT 0 NOT NULL COMMENT 'notify_ago',
created datetime NOT NULL COMMENT '作成時刻',
updated datetime NOT NULL COMMENT '更新時刻',
deleted datetime COMMENT '削除時刻',
-- 削除フラグ(0=未削除,1=削除済み)
is_deleted tinyint unsigned DEFAULT 0 NOT NULL COMMENT '削除フラグ',
PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = '材料発注関連マスタ' DEFAULT CHARACTER SET utf8;
";

$SQL3 = "
CREATE TABLE `units` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
  `unit_name` varchar(32) NOT NULL COMMENT 'unit_name',
  `created` datetime NOT NULL COMMENT '作成時刻',
  `updated` datetime NOT NULL COMMENT '更新時刻',
  `deleted` datetime DEFAULT NULL COMMENT '削除時刻',
  `is_deleted` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='単位マスタ';
";
        $this->db->query($SQL1);
        $this->db->query($SQL2);
        $this->db->query($SQL3);


// CSV並び
// "発注用名称",
// "分量計算用単位",
// "最小発注単位分量",
// "発注用単位",
// "仕入先",
// "お知らせ対象",
// "お知らせ日数",

        $suppliers  = [];
        $units      = [];
        $food_order = [];

        // CSV分解開始
        if ( !empty($CSV_PATH) )
        {
            $CSV = fopen('csv_data/' . $CSV_PATH, 'r');
            fgetcsv($CSV);
            while( false !== ($line = fgetcsv($CSV)) )
            {
                // 単位をユニークで取得
                $units[$line[3]] = [
                    'unit_name' => $line[3],
                    'created'   => date('Y-m-d H:i:s'),
                    'updated'   => date('Y-m-d H:i:s')
                ];
                $units[$line[1]] = [
                    'unit_name' => $line[1],
                    'created'   => date('Y-m-d H:i:s'),
                    'updated'   => date('Y-m-d H:i:s')
                ];

                // 発注先をユニークで取得
                $suppliers[$line[4]] = [
                    'place_id'      => $PLACE_ID,
                    'supplier_name' => $line[4],
                    'created'       => date('Y-m-d H:i:s'),
                    'updated'       => date('Y-m-d H:i:s')
                ];
            }

            // 発注先DB保存
            sort($suppliers);
            $this->db->insert_batch('suppliers', $suppliers);

            // 単位DB保存
            sort($units);
            $this->db->insert_batch('units', $units);

            // 入れた発注先, 単位DB取得
            $unit_result = $this->db->get('units')->result();
            $unit_arr = [];
            foreach ( $unit_result as $u )
            {
                $unit_arr[$u->unit_name] = $u->id;
            }

            $suppliers_result = $this->db->get('suppliers')->result();
            $suppliers_arr = [];
            foreach ( $suppliers_result as $s )
            {
                $suppliers_arr[$s->supplier_name] = $s->id;
            }

            rewind($CSV);
            fgetcsv($CSV);
            while( false !== ($line = fgetcsv($CSV)) )
            {

                // 材料データ投入
                $food_order = [
                    'place_id'      => $PLACE_ID,
                    'supplier_id'   => $suppliers_arr[$line[4]],
                    'order_name'    => $line[0],
                    'order_minimum_amount' => $line[2],
                    'order_unit_id' => $unit_arr[$line[3]],
                    'notify_ago'    => $line[6],
                    'created'       => date('Y-m-d H:i:s'),
                    'updated'       => date('Y-m-d H:i:s')
                ];

                $this->db->insert('foodstuff_orders', $food_order);
            }
        }


        return true;

    }



    /*
     * レシピデータ取り込み
     * 対象テーブル recipes, recipe_foodstuffs, recipe_foodstuff_orders;
     * $ php index.php csvimport recipe iwate_recipe.csv 1
     */
    public function recipe($CSV_PATH = '', $PLACE_ID = 1)
    {

        if ( $this->db->table_exists('recipes') )
        {
            $this->db->query('delete from recipes');
            $this->db->query('drop table recipes');
        }
        if ( $this->db->table_exists('recipe_foodstuffs') )
        {
            $this->db->query('delete from recipe_foodstuffs');
            $this->db->query('drop table recipe_foodstuffs');
        }
        if ( $this->db->table_exists('recipe_foodstuff_orders') )
        {
            $this->db->query('delete from recipe_foodstuff_orders');
            $this->db->query('drop table recipe_foodstuff_orders');
        }

        $SQL1 = "CREATE TABLE recipes
(
id int unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
recipe_type tinyint DEFAULT 0 NOT NULL COMMENT 'レシピ種別',
recipe_name text COMMENT 'レシピ名称',
calorie float COMMENT 'カロリー',
mealtime_type tinyint unsigned DEFAULT 0 COMMENT '食事種別',
recipe_steps_html text NOT NULL COMMENT 'レシピ手順内容',
image_path varchar(512) COMMENT '画像パス',
created datetime NOT NULL COMMENT '作成時刻',
updated datetime NOT NULL COMMENT '更新時刻',
deleted datetime COMMENT '削除時刻',
-- 削除フラグ(0=未削除,1=削除済み)
is_deleted tinyint unsigned DEFAULT 0 NOT NULL COMMENT '削除フラグ',
PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = 'レシピマスタ' DEFAULT CHARACTER SET utf8;
";
        $SQL2 = "CREATE TABLE recipe_foodstuffs
(
id int unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
recipe_id int unsigned COMMENT 'recipe_id',
-- 合わせ調味料等のグルーピング番号(0=なし,!0=グループ)
-- 画面上に表示する場合、1=A, 2=B となるように表示する。
combine_group_no tinyint DEFAULT 0 NOT NULL COMMENT '合わせグループNo',
-- 適量など、マッチしない文言の場合は 0 を格納する。
unit_id int unsigned DEFAULT 0 NOT NULL COMMENT 'unit_id',
amount float DEFAULT 0 COMMENT '量',
name text COMMENT '調味料名称',
-- テキスト表示用(数量等で表現できない場合に使用する)
amount_text text COMMENT '量(テキスト)',
created datetime NOT NULL COMMENT '作成時刻',
updated datetime NOT NULL COMMENT '更新時刻',
deleted datetime COMMENT '削除時刻',
-- 削除フラグ(0=未削除,1=削除済み)
is_deleted tinyint unsigned DEFAULT 0 NOT NULL COMMENT '削除フラグ',
PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = 'レシピ材料マスタ' DEFAULT CHARACTER SET utf8;
";
        $SQL3 = "CREATE TABLE recipe_foodstuff_orders
(
id int unsigned NOT NULL AUTO_INCREMENT COMMENT 'id',
recipe_foodstuff_id int unsigned COMMENT 'レシピ材料ID',
place_id int unsigned COMMENT '拠点ID',
foodstuff_order_id int unsigned COMMENT '発注材料ID',
created datetime NOT NULL COMMENT '作成時刻',
updated datetime NOT NULL COMMENT '更新時刻',
deleted datetime COMMENT '削除時刻',
-- 削除フラグ(0=未削除,1=削除済み)
is_deleted tinyint unsigned DEFAULT 0 NOT NULL COMMENT '削除フラグ',
PRIMARY KEY (id)
) ENGINE = InnoDB COMMENT = 'レシピ材料発注関連マスタ' DEFAULT CHARACTER SET utf8;
";

        $this->db->query($SQL1);
        $this->db->query($SQL2);
        $this->db->query($SQL3);


        // CSV取り込み
        if ( !empty($CSV_PATH) )
        {
            // CONST 定義っぽいの
            $mealtime_type = ['夕食' => 2, '朝食' => 1];
            $recipe_type   = [
                '主食'     => 0,
                '主菜'     => 1,
                '副食'     => 2,
                'スープ'   => 3,
                '汁'       => 3,
                'デザート' => 4,
                'サラダ'   => 5,
            ];
            $combine = [''=>0, 'A'=>1, 'B'=>2, 'C'=>3, 'D'=>4, 'E'=>5, 'F'=>6];

            // 入れた発注先, 単位DB取得
            $unit_result = $this->db->get('units')->result();
            $unit_arr = [];
            foreach ( $unit_result as $u ) {
                $unit_arr[$u->unit_name] = $u->id;
            }


            $CSV = fopen('csv_data/' . $CSV_PATH, 'r');
            fgetcsv($CSV);
            $recipes                 = [];
            $recipe_foodstuffs       = [];
            $recipe_foodstuff_orders = [];

            // レシピデータ分解
            while( false !== ($line = fgetcsv($CSV)) )
            {
                // recipes 手順があったものはレシピデータとみなす
                if ( !empty($line[12]) && "\n" != $line[12] )
                {
                    $recipe_html = '';


                    for ( $i = 12; $i <= 20; $i++ )
                    {
                        if ( !empty($line[$i]) )
                        {
                            $line[$i] = str_replace('{{', '<span class="red">', $line[$i]);
                            $line[$i] = str_replace('}}', '</span>', $line[$i]);
                            $line[$i] = str_replace('[[', '<span class="gray">', $line[$i]);
                            $line[$i] = str_replace(']]', '</span>', $line[$i]);
                            $recipe_html .= '<li>' . $line[$i] . '</li>';
                        }
                    }
                    $recipes[] = [
                        'recipe_type'       => $recipe_type[$line[1]],
                        'recipe_name'       => $line[0],
                        'calorie'           => !empty($line[2]) ? $line[2] : '',
                        'mealtime_type'    => $mealtime_type[$line[3]],
                        'recipe_steps_html' => $recipe_html,
                        'image_path'        => $line[4],
                        'created'           => date('Y-m-d H:i:s'),
                        'updated'           => date('Y-m-d H:i:s'),
                    ];
                }


                $recipe_foodstuffs[] = [
                    'recipe_id'        => count($recipes),
                    'combine_group_no' => $combine[$line[8]],
                    'unit_id'          => $unit_arr[$line[10]],
                    'amount'           => $line[9],
                    'name'             => $line[5],
                    'amount_text'      => $line[11],
                    'created'          => date('Y-m-d H:i:s'),
                    'updated'          => date('Y-m-d H:i:s')
                ];

                if ( !empty($line[6]) )
                {
                    $trimline = rtrim($line[6]);
                    $stuff_order_id = $this->db->where('order_name', $trimline)
                        ->get('foodstuff_orders')->result();

                    // 入らなかった発注データ表示
                    if ( empty($stuff_order_id) ) {
                        var_dump($trimline);
                    }


                }

                if ( !empty($stuff_order_id[0]) )
                {
                    $recipe_foodstuff_orders[] = [
                        'recipe_foodstuff_id' => count($recipe_foodstuffs),
                        'place_id'            => $PLACE_ID,
                        'foodstuff_order_id'  => $stuff_order_id[0]->id,
                        'created'             => date('Y-m-d H:i:s'),
                        'updated'             => date('Y-m-d H:i:s'),
                    ];
                }
            }

            $this->db->insert_batch('recipes', $recipes);
            $this->db->insert_batch('recipe_foodstuffs', $recipe_foodstuffs);
            $this->db->insert_batch('recipe_foodstuff_orders', $recipe_foodstuff_orders);

        }
        // recipes, recipe_foodstuffs, recipe_foodstuff_orders;
    }
}

