<!doctype html>

<html lang="ja">
<head>
<meta charset="utf-8">
<meta name="description" content="The HTML5 Herald">
<meta name="author" content="SitePoint">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>The HTML5 Herald</title>

<link rel="stylesheet" href="https://gitcdn.link/repo/Chalarangelo/mini.css/master/dist/mini-default.min.css">

<!--[if lt IE 9]>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
<![endif]-->
</head>

<body>

<header class="sticky">
  <!-- This might seem a bight hacky but it works great -->
  <div class="col-sm col-md-10 col-md-offset-1">
    <a href="#" role="button">Home</a>
    <a href="#" role="button">About</a>
    <a href="#" role="button">Contact</a>
  </div>
</header>



